#pragma once
#include <vector>

struct tVertex {
	float fX, fY, fZ;	// Coordinates
	float fNX, fNY, fNZ;	// Normal
	float fU, fV;		// Texture Coordinates

	bool operator==(const tVertex& other);
	bool operator!=(const tVertex& other);
};

struct tTriangle {
	unsigned int uIndices[3]; // these create a triangle from 3 vertices in the unique vertex list.
};

class CMesh {
public:
	std::string m_strName; // This is the name of the mesh.
	std::vector<std::string> m_vTextureNames; // These are the textures that are used in this mesh.
	std::vector<tVertex> m_vUniqueVerts; // These are all the unique vertices in this mesh.
	std::vector<tTriangle> m_vTriangles; // These are the triangles that make up the mesh.

	std::tuple<bool, int> vertexIsUnique(const tVertex& other);
};
