#include "ExporterIncludes.h"
#include <tuple>

bool tVertex::operator==(const tVertex & other) {
	return
		fX == other.fX &&
		fY == other.fY &&
		fZ == other.fZ &&
		fNX == other.fNX &&
		fNY == other.fNY &&
		fNZ == other.fNZ &&
		fU == other.fU &&
		fV == other.fV;
}

bool tVertex::operator!=(const tVertex & other) {
	return
		fX  != other.fX  ||
		fY  != other.fY  ||
		fZ  != other.fZ  ||
		fNX != other.fNX ||
		fNY != other.fNY ||
		fNZ != other.fNZ ||
		fU  != other.fU  ||
		fV  != other.fV;
}

std::tuple<bool, int> CMesh::vertexIsUnique(const tVertex & other)
{
	for (int i = 0; i < m_vUniqueVerts.size(); ++i) {
		if (m_vUniqueVerts[i] == other) {
			return std::make_tuple(false, i);
		}
	}

	return std::make_tuple(true, -1);
}
